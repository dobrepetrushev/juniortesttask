<?php

include './php/Product.php';

?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <!-- JQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>  
    <link rel="stylesheet" href="css/style.css">  

    <title>Add product</title>
  </head>
  <body>

<div class="container py-5">
    
<form  method="post" id="product_form" class="needs-validation" novalidate > 

  <div class="holder-button d-flex border-bottom justify-content-between align-items-center py-3">
        
    <div class="left-side">
      <h4>Add Product</h4>
    </div>
      <div class="right-side d-flex align-items-center">
          <button type="submit" class="btn btn-primary btn-sm">Save</button>
          <a href="/index.php" type="button" id="delete-product-btn" class="btn btn-secondary btn-sm">CANCEL</a>
      </div>            
  </div>
   
    <div class="form-group row">
      <label for="sku" class="col-sm-2 col-form-label mt-5">SKU</label>
      <div class="col-sm-10">
          <input type="text" class="form-control mt-5" id="sku" name="sku" placeholder="SKU" required>
      </div>
    </div>
    <div class="form-group row">
      <label for="name" class="col-sm-2 col-form-label">Name</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" id="name" name="name" placeholder="Name" required>
      </div>
    </div>
    <div class="form-group row">
      <label for="price" class="col-sm-2 col-form-label">Price</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" id="price" name="price" placeholder="Price" required>
      </div>
    </div>
  
    <div class="form-group d-flex ">
      <label for="productType" class="form-label-sm col-sm-2 pl-1 col-form-label">Type switcher</label>
      <select id="productType" class="form-select-sm col-sm-10" name="productType" required>
        <option value="" selected>Choose...</option>
        <option id="DVD" value="1">DVD</option>
        <option id="Book" value="2">Book</option>
        <option id="Furniture" value="3">Furniture</option>
      </select>
    </div>
    <!-- label for DVD  -->
      <div class="form-group row dvd_label mb-2 d-none" id="sizeHolder">
          <label for="size" class="col-sm-2 col-form-label">Size</label>
          <div class="col-sm-10">
              <input type="text" class="form-control" id="size" name="size" placeholder="Size DVD">
          </div>
      </div>
    <!-- label for books in kg -->
      <div class="form-group row books_label d-none" id="weightHolder" >
        <label for="weight" class="col-sm-2 col-form-label">Size in Kg </label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="weight" name="weight" placeholder="Size in Kg">
        </div>
      </div>
    <!-- label for Furniture -->
      <div class="form-group row books_label d-none " id="furnitureHolder">
        <label for="Height" class="col-sm-2 col-form-label">Height </label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="height" name="height" placeholder="Height">
            </div>
        <label for="Width" class="col-sm-2 col-form-label">Width </label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="width"  name="width" placeholder="Width">
        </div>
        <label for="length" class="col-sm-2 col-form-label">Length </label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="length" name="length" placeholder="length">
        </div>
      </div>
</form>

</div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="./js/script.js"></script>
  </body>
</html>