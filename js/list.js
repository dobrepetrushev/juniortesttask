// delete selected records
$('#delete-product-btn').on('click', function(e) { 
	var products = [];  
	$(".delete-checkbox:checked").each(function() {  
		products.push($(this).data('pro-id'));
	});	
	
  var selected_values = products.join(","); 
  $.ajax({ 
      type: "POST",  
      url: "./php/delete_action.php",  
      cache:false,  
      data: 'pro-id='+selected_values,  
      success: function(response) {	
         window.location.href = 'index.php';
         var pro_id = response.split(",");
         
         for (var i=0; i < pro_id.length; i++ ) {		
            var idProduct = pro_id[i].trim();				
            $("#"+idProduct).remove();
         }									
      }   
  });				
	
});

