// Type switcher add remove display none classes 
document.getElementById('productType').addEventListener('change', function(){
    var value = this.value;
    
    if(value == 1){       
        document.getElementById('sizeHolder').classList.remove('d-none');
        document.getElementById('weightHolder').classList.add('d-none');
        document.getElementById('furnitureHolder').classList.add('d-none');
    }else if(value == 2){
        document.getElementById('sizeHolder').classList.add('d-none');
        document.getElementById('weightHolder').classList.remove('d-none');
        document.getElementById('furnitureHolder').classList.add('d-none');
    }else if(value == 3){
        document.getElementById('sizeHolder').classList.add('d-none');
        document.getElementById('weightHolder').classList.add('d-none');
        document.getElementById('furnitureHolder').classList.remove('d-none');
    }else {
        document.getElementById('sizeHolder').classList.add('d-none');
        document.getElementById('weightHolder').classList.add('d-none');
        document.getElementById('furnitureHolder').classList.add('d-none');
    }
 
});
// check fields with botostrap 
(function () {
    'use strict'
  
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.querySelectorAll('.needs-validation')
  
    // Loop over them and prevent submission
    Array.prototype.slice.call(forms)
      .forEach(function (form) {
        form.addEventListener('submit', function (event) {
          if (!form.checkValidity()) {
            event.preventDefault()
            event.stopPropagation()
          }else {
            ajaxWrite(this)
          }

          
  
          form.classList.add('was-validated')
        }, false)
      })
  })()
 
// Take elements from product form and post in Product.php
function ajaxWrite(form){
    const sku = form.elements['sku'].value;
    const name = form.elements['name'].value;
    const price = form.elements['price'].value;
    const productType = form.elements['productType'].value;
    const size = form.elements['size'].value;
    const weight = form.elements['weight'].value;
    const height = form.elements['height'].value;
    const width = form.elements['width'].value;
    const length = form.elements['length'].value;
    
     $.ajax({
         type: 'POST',
         url: './php/Product.php',
         data: {
            sku:sku,
            name:name,
            price:price,
            productType:productType,
            size:size,
            weight:weight,
            height:height,
            width:width,
            length:length
        },
        success: function(response) {
            window.location.href = 'index.php';

        },
        error: function() {
             
        }
     })
}
  

 