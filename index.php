<?php

    include './php/Product.php';
?>


<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">
    <title>Product Page</title>
  </head>
  <body>

    <div class="container py-5">

      <div class="holder-button d-flex border-bottom justify-content-between align-items-center py-3">
        <div class="left-side">
            <h4>Product List</h4>
        </div>
        <div class="right-side d-flex align-items-center">
          <a href="/Product_add.php" type="button" class="btn btn-primary btn-sm">ADD</a>
          <a type="button" id="delete-product-btn" class="btn btn-secondary btn-sm">MASS DELETE</a>
        </div>       
      </div>
     
      <div class="row row-cols-1 row-cols-md-4 g-4 mt-4">
        <?php foreach($products as $product){?>      
          <div class="col" id=<?php echo $product['id']; ?>>
            <div class="card p-4">
                <input type="checkbox" class="delete-checkbox" data-pro-id="<?php echo $product['id']; ?>">
                <div class="card-body text-center align-middle">
                  <h5 class="card-title"><?php echo htmlspecialchars($product['name']); ?></h5>
                  <span><?php echo htmlspecialchars($product['sku']); ?></span></br>
                  <span><?php echo htmlspecialchars($product['name']); ?></span></br>
                  <span><?php echo htmlspecialchars($product['price']); ?></span></br>
                  <span><?php echo htmlspecialchars($product['size']); ?></span></br>
                  <span><?php echo htmlspecialchars($product['weight']); ?></span></br>
                  <span><?php echo htmlspecialchars($product['height'] . " X " . $product['width'] ." X ". $product['length']);  ?></span>
                </div>
            </div>
          </div>
        <?php } ?>
      </div>
    </div>
    
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="./js/list.js"></script>
  </body>
</html>